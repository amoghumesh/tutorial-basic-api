Basic examples to get started
-----------------------------

This directory contains the following examples to get started,
and we recommend that beginners walk through these examples in the same
order as they are presented:

1. point-to-point-two-nodes.cc
==============================

General documentation:

.. literalinclude:: point-to-point-two-nodes.cc
   :language: cpp
   :start-after: start-general-documentation
   :end-before: end-general-documentation

The network topology diagram is as follows: 

.. literalinclude:: point-to-point-two-nodes.cc
   :language: cpp
   :start-after: start-network-topology
   :end-before: end-network-topology

Creation of nodes and devices for a point-to-point link: 

.. literalinclude:: point-to-point-two-nodes.cc
   :language: cpp
   :start-after: create-nodes-and-devices
   :end-before: install-protocol-stack

Install the requisite protocol stacks, then assign IP addresses to the devices 
on our nodes.

.. literalinclude:: point-to-point-two-nodes.cc
   :language: cpp
   :start-after: install-protocol-stack
   :end-before: use-ping-application

Initialize the source and destination addresses to test connectivity, and set 
up a Ping application on one of the nodes.

.. literalinclude:: point-to-point-two-nodes.cc
   :language: cpp
   :start-after: use-ping-application
   :end-before: run-simulation

Run the simulation. Once the events in the ping application are executed, the 
simulator stops and is cleaned up.

.. literalinclude:: point-to-point-two-nodes.cc
   :language: cpp
   :start-after: run-simulation
