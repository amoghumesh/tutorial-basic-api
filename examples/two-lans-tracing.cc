/*
 * Copyright (c) 2023 NITK Surathkal
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Kavya Bhat <kavyabhat@gmail.com>
 *          Devaansh Kumar <devaanshk840@gmail.com>
 */

/*
 * Two LANs with 4 nodes each connected via two routers, asymmetric link
 * bandwidth and delay, IPv4 addresses, Ping application.
 */

/*
 * This example extends from two-lans-point-to-point.cc to show how command
 * line arguments and tracing can be utilized.
 * 
 * The program sets up a command-line argument parser to allow users to specify 
 * simulation parameters via command-line arguments and also configures logging
 * to capture and display log messages during the simulation.
 * 
 * The program also demonstrates how tracing works. Trace sources is connected
 * to trace sinks and analysed later. This program records tracing in pcap
 * format, ascii format and console output. 
 * 
 * Network topology:
 *                                                                                                
 *                       10.1.1.0              10.1.2.0              10.1.3.0                       
 *                     5Mbps, 2ms -->        5Mbps, 2ms -->        5Mbps, 2ms -->                   
 *  n5  n4  n3  n2  ------------------ n0 ------------------ n1 ------------------  n6  n7  n8  n9 
 *   |  |   |   |     <-- 10Mbps, 2ms       <-- 10Mbps, 2ms      <-- 10Mbps, 2ms    |   |   |   |  
 * ================                                                                ================ 
 *      LAN 1              link 1                link 2                link 3            LAN 2      
 *    10.1.0.0         point-to-point        point-to-point        point-to-point       10.1.0.0    
 * 
 * IPv4 addresses:  
 * ... : LAN   |   <-> : p2p
 * 10.1.0.4 ... 10.1.0.1 / 10.1.1.1   <->  10.1.1.2 / 10.1.2.1  <->  10.1.2.2 / 10.1.3.1  <-> 10.1.3.2 / 10.1.4.1 ... 10.1.4.4 
 * 
 * Five Ping packets are sent from n3 to n7 and traces sources are set
 * on n3, r0 and n7. 
 * 
 * The output will look like this:
 * PING 10.1.4.4 - 56 bytes of data; 84 bytes including ICMP and IPv4 headers.
 * m_seq=0
 * TxBegin at 1.009
 * TxBegin at 1.01101
 * MacTx at 1.01416
 * RxEnd at 1.02144
 * RxEnd at 1.02345
 * RxEnd at 1.03246
 * TxBegin at 1.04169
 * Received Echo Reply size  = 64 bytes from 10.1.4.4 id =  48879 seq = 0 TTL = 60
 * 64 bytes from 10.1.4.4: icmp_seq=0 ttl=60 time=43.7 ms
 * m_seq=1
 * TxBegin at 2
 * MacTx at 2.00315
 * RxEnd at 2.00843
 * Received Echo Reply size  = 64 bytes from 10.1.4.4 id =  48879 seq = 1 TTL = 60
 * 64 bytes from 10.1.4.4: icmp_seq=1 ttl=60 time=16.652 ms
 * m_seq=2
 * TxBegin at 3
 * MacTx at 3.00315
 * RxEnd at 3.00843
 * Received Echo Reply size  = 64 bytes from 10.1.4.4 id =  48879 seq = 2 TTL = 60
 * 64 bytes from 10.1.4.4: icmp_seq=2 ttl=60 time=16.652 ms
 * m_seq=3
 * TxBegin at 4
 * MacTx at 4.00315
 * RxEnd at 4.00843
 * Received Echo Reply size  = 64 bytes from 10.1.4.4 id =  48879 seq = 3 TTL = 60
 * 64 bytes from 10.1.4.4: icmp_seq=3 ttl=60 time=16.652 ms
 * m_seq=4
 * TxBegin at 5
 * MacTx at 5.00315
 * RxEnd at 5.00843
 * Received Echo Reply size  = 64 bytes from 10.1.4.4 id =  48879 seq = 4 TTL = 60
 * 64 bytes from 10.1.4.4: icmp_seq=4 ttl=60 time=16.652 ms
 * 
 * --- 10.1.4.4 ping statistics ---
 * 5 packets transmitted, 5 received, 0% packet loss, time 4016ms
 * rtt min/avg/max/mdev = 16/21.4/43/12.07 ms
 * 
 * The example program produces output for pcap tracing in two-lans-tracing-*.pcap
 * files and ascii tracing in two-lans-tracing.tr file.
 */

#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-apps-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include <iostream>

//////////////////////////////////////////////////////////////////////////////////////////////////////
//                                         Network Topology                                         //
//                                                                                                  //
//     10.1.0.0          10.1.1.0              10.1.2.0              10.1.3.0          10.1.4.0     //
//   100Mbps, 1ms      5Mbps, 2ms -->        5Mbps, 2ms -->        5Mbps, 2ms -->    100Mbps, 1ms   //
//  n5  n4  n3  n2 ------------------- n0 ------------------- n1 ------------------ n6  n7  n8  n9  //
//   |  |   |   |     <-- 10Mbps, 2ms       <-- 10Mbps, 2ms      <-- 10Mbps, 2ms    |   |   |   |   //
// ================                                                                ================ //
//      LAN 1              link 1                link 2                link 3            LAN 2      //
//                     point-to-point        point-to-point        point-to-point                   //
//////////////////////////////////////////////////////////////////////////////////////////////////////

using namespace ns3;

/**
 * PhyTxBegin callback
 * It is fired when a packet has begun transmitting over the channel.
 * \param p The sent packet.
 */
static void
TxBegin(Ptr<const Packet> p)
{
    std::cout << "TxBegin at " << Simulator::Now().GetSeconds() << std::endl;
}

/**
 * PhyRxEnd callback
 * It is fired when a packet has been completely received by the device.
 * \param p The received packet.
 */
static void
RxEnd(Ptr<const Packet> p)
{
     std::cout << "RxEnd at " << Simulator::Now().GetSeconds() << std::endl;
}

/**
 * MacTx callback
 * It is fired when a packet has arrived for transmission by this device.
 * \param p The received packet.
 */
static void
MacTx(Ptr<const Packet> p)
{
     std::cout << "MacTx at " << Simulator::Now().GetSeconds() << std::endl;
}

int
main(int argc, char* argv[])
{
    bool pcapTracing = false;
    bool asciiTracing = false;
    bool verbose = false;

    // Allows to parse command line arguments
    CommandLine cmd;

    /*
     * The 'AddValue' API adds a program argument. It takes the name of
     * program-supplied argument, the help text displayed when program is run
     * with "--help" argument and a value that will reference the varible
     * whose value is to be set.
     *
     * Command line argumets can be invoked in the following way:
     * ./ns3 run "two-lans-tracing.cc --pcapTracing=true --asciiTracing=true --verbose=true"
     *
     * For boolean arguments if only the argument is mentioned then the value
     * will be toggled.
     */
    cmd.AddValue("pcapTracing", "Enable PCAP Tracing", pcapTracing);
    cmd.AddValue("asciiTracing", "Enable ASCII Tracing", asciiTracing);
    cmd.AddValue("verbose", "Make output verbose", verbose);
    cmd.Parse(argc, argv);

    /*
     * LogComponentEnable enables logging output associated with the specified
     * component with the given Log Level.
     */
    if (verbose)
    {
        LogComponentEnable("Ping", LOG_LEVEL_INFO);
    }

    // Create the 10 nodes n0, n1, n2... n9
    NodeContainer nodes;
    nodes.Create(10);

    // Add the router nodes n0, n1 to the container routerNodes
    NodeContainer routerNodes;
    routerNodes.Add(nodes.Get(0));
    routerNodes.Add(nodes.Get(1));

    // Add the CSMA LAN 1 nodes n2, n3, n4, n5 to the container csmaLan1Nodes
    NodeContainer csmaLan1Nodes;
    csmaLan1Nodes.Add(nodes.Get(2));
    csmaLan1Nodes.Add(nodes.Get(3));
    csmaLan1Nodes.Add(nodes.Get(4));
    csmaLan1Nodes.Add(nodes.Get(5));

    // Add the CSMA LAN 2 nodes n6, n7, n8, n9 to the container csmaLan2Nodes
    NodeContainer csmaLan2Nodes;
    csmaLan2Nodes.Add(nodes.Get(6));
    csmaLan2Nodes.Add(nodes.Get(7));
    csmaLan2Nodes.Add(nodes.Get(8));
    csmaLan2Nodes.Add(nodes.Get(9));

    /*
     * Create a CsmaHelper. The attribute DataRate is set to 100Mbps and
     * Delay is set to 1ms.
     */
    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", StringValue("1ms"));

    // Create NetDeviceContainers for the csma devices on lan1 and lan2.
    NetDeviceContainer csmaLan1Devices;
    csmaLan1Devices = csma.Install(csmaLan1Nodes);

    NetDeviceContainer csmaLan2Devices;
    csmaLan2Devices = csma.Install(csmaLan2Nodes);

    // Creates a PointToPointHelper, with the attribute Delay set to 2ms.
    PointToPointHelper pointToPoint;
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    // Create NetDeviceContainers for the devices on link1, link2 and link3.
    NetDeviceContainer link1Devices;
    link1Devices = pointToPoint.Install(csmaLan1Nodes.Get(0), routerNodes.Get(0));
    link1Devices.Get(0)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("5Mbps"));
    link1Devices.Get(1)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("10Mbps"));

    NetDeviceContainer link2Devices;
    link2Devices = pointToPoint.Install(routerNodes);
    link2Devices.Get(0)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("5Mbps"));
    link2Devices.Get(1)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("10Mbps"));

    NetDeviceContainer link3Devices;
    link3Devices = pointToPoint.Install(routerNodes.Get(1), csmaLan2Nodes.Get(0));
    link3Devices.Get(0)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("5Mbps"));
    link3Devices.Get(1)->GetObject<PointToPointNetDevice>()->SetAttribute("DataRate",
                                                                          StringValue("10Mbps"));

    // Install the internet stack on all the nodes
    InternetStackHelper stack;
    stack.Install(csmaLan1Nodes);
    stack.Install(csmaLan2Nodes);
    stack.Install(routerNodes);

    // Assign IPv4 addresses to all the interfaces.
    // csma LAN 1
    Ipv4AddressHelper csmaLan1Address;
    csmaLan1Address.SetBase("10.1.0.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaLan1Interfaces = csmaLan1Address.Assign(csmaLan1Devices);

    // link 1
    Ipv4AddressHelper link1Address;
    link1Address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer link1Interfaces = link1Address.Assign(link1Devices);

    // link 2
    Ipv4AddressHelper link2Address;
    link2Address.SetBase("10.1.2.0", "255.255.255.0");
    Ipv4InterfaceContainer link2Interfaces = link2Address.Assign(link2Devices);

    // link 3
    Ipv4AddressHelper link3Address;
    link3Address.SetBase("10.1.3.0", "255.255.255.0");
    Ipv4InterfaceContainer link3Interfaces = link3Address.Assign(link3Devices);

    // csma LAN 2
    Ipv4AddressHelper csmaLan2Address;
    csmaLan2Address.SetBase("10.1.4.0", "255.255.255.0");
    Ipv4InterfaceContainer csmaLan2Interfaces = csmaLan2Address.Assign(csmaLan2Devices);

    // Build and populate routing table on all nodes in the simulation.
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    /*
     * Enable pcap(packet capture) output on each device. A filename prefix is
     * provided as an argument to the 'EnablePcapAll' helper.
     * .pcap files can be read using tools like tcpdump.
     */
    if (pcapTracing)
    {
        csma.EnablePcapAll("two-lans-tracing-csma");
        pointToPoint.EnablePcapAll("two-lans-tracing-p2p");
    }

    /*
     * Enable ASCII Tracing using the AsciiTraceHelper.
     * The 'CreateFileStream' helper creates and initializes an output stream
     * object to which we write the traced bits.
     * The 'EnableAsciiAll' helper enables ascii trace output on each device.
     */
    if (asciiTracing)
    {
        AsciiTraceHelper asciiTraceHelper;
        Ptr<OutputStreamWrapper> stream = asciiTraceHelper.CreateFileStream("two-lans-tracing.tr");
        csma.EnableAsciiAll(stream);
        pointToPoint.EnableAsciiAll(stream);
    }

    // Ping from a device in LAN 1 to a device in LAN 2
    Address destination = csmaLan2Interfaces.GetAddress(3); // destination: n9
    Address source = csmaLan1Interfaces.GetAddress(3);      // source: n2

    // Create a ping application with n9 as destination and n2 as source.
    PingHelper pingHelper(destination, source);
    pingHelper.SetAttribute("Interval", TimeValue(Seconds(1.0)));
    pingHelper.SetAttribute("Size", UintegerValue(56));
    pingHelper.SetAttribute("Count", UintegerValue(5));

    /*
     * Trace sources allow you to collect and record information about events
     * that occur during a network simulation.
     * The 'TraceConnectWithoutContext' API takes the name of target trace
     * source and a user defined callback function that will fire whenever
     * the event occurs.
     */
    csmaLan1Devices.Get(3)->TraceConnectWithoutContext("PhyTxBegin", MakeCallback(&TxBegin));
    csmaLan2Devices.Get(3)->TraceConnectWithoutContext("PhyRxEnd", MakeCallback(&RxEnd));
    link2Devices.Get(0)->TraceConnectWithoutContext("MacTx", MakeCallback(&MacTx));

    // Install the ping application on node n2.
    ApplicationContainer apps = pingHelper.Install(csmaLan1Nodes.Get(3));
    apps.Start(Seconds(1));
    apps.Stop(Seconds(6));

    /*
     * Set the stop time for the simulator. Run the simulation, and invoke
     * 'Destroy' at the end of the simulation.
     */
    Simulator::Stop(Time(Seconds(6.0)));
    Simulator::Run();
    Simulator::Destroy();
    return 0;
}
